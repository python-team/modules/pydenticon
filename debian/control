Source: pydenticon
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools (>= 0.6.24),
 python3-pil
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/azaghal/pydenticon
Vcs-Git: https://salsa.debian.org/python-team/packages/pydenticon.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pydenticon

Package: python3-pydenticon
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends}
Description: deterministically generate identicons in Python 3
 Pydenticon is a small utility library that can be used for deterministically
 generating identicons based on the hash of provided data.
 .
 The implementation is a port of the Sigil identicon implementation.
 .
 Pydenticon provides a couple of extensions of its own when compared to the
 original Sigil implementation, like:
 .
 * Ability to supply custom digest algorithms (allowing for larger identicons if
   digest provides enough entropy).
 * Ability to specify a rectangle for identicon size.
